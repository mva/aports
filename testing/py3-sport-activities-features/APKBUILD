# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-sport-activities-features
pkgver=0.3.7
pkgrel=0
pkgdesc="A minimalistic toolbox for extracting features from sport activity files"
url="https://github.com/firefly-cpp/sport-activities-features"
arch="noarch !ppc64le !s390x !riscv64" # py3-niaaml
license="MIT"
depends="
	python3
	py3-dotmap
	py3-geopy
	py3-geotiler
	py3-gpxpy
	py3-matplotlib
	py3-niaaml
	py3-overpy
	py3-requests
	py3-tcxreader
	"
checkdepends="python3-dev py3-pytest"
makedepends="py3-build py3-poetry-core py3-wheel py3-installer"
subpackages="$pkgname-doc"
source="https://github.com/firefly-cpp/sport-activities-features/archive/$pkgver/sport-activities-features-$pkgver.tar.gz"
builddir="$srcdir/sport-activities-features-$pkgver"

build() {
	GIT_DIR="$builddir" python3 -m build --no-isolation --wheel
}

check() {
	python3 -m pytest -k "not test_weather"
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/sport_activities_features-$pkgver-py3-none-any.whl

	install -Dm644 docs/preprints/A_minimalistic_toolbox.pdf -t "$pkgdir"/usr/share/doc/$pkgname
}

sha512sums="
d5e8fef3e12e03b16cea34562f74b8a666782f3eec30a3ac6a0ac78e35298a1fb283ca4a8d0f642e4ab6b0723a9150d4e83e4f4a31cb40dbd947139971f19563  sport-activities-features-0.3.7.tar.gz
"
