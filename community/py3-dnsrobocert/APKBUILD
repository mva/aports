# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-dnsrobocert
pkgver=3.22.0
pkgrel=0
pkgdesc="A tool to manage your DNS-challenged TLS certificates"
url="https://dnsrobocert.readthedocs.io/en/latest/"
arch="noarch"
license="MIT"
depends="
	certbot
	py3-acme
	py3-boto3
	py3-cffi
	py3-cryptography
	py3-colorama
	py3-coloredlogs
	py3-dnspython
	py3-dns-lexicon
	py3-jsonschema
	py3-localzone
	py3-lxml
	py3-openssl
	py3-pem
	py3-schedule
	py3-softlayer
	py3-softlayer-zeep
	py3-tldextract
	py3-xmltodict
	py3-yaml
	"
makedepends="
	py3-build
	py3-installer
	py3-poetry-core
	"
checkdepends="pebble py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://github.com/adferrand/dnsrobocert/archive/v$pkgver.tar.gz
	pebble.patch
	"
builddir="$srcdir/dnsrobocert-$pkgver"

build() {
	# XXX: hack for poetry to not ignore files
	GIT_DIR=. python3 -m build --no-isolation --wheel
}

check() {
	python3 -m installer -d testenv \
		dist/dnsrobocert-$pkgver-py3-none-any.whl
	local sitedir="$(python3 -c 'import site;print(site.getsitepackages()[0])')"
	PYTHONPATH="$PWD/testenv/$sitedir" python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/dnsrobocert-$pkgver-py3-none-any.whl
}

sha512sums="
6c39e1509e02c37e570091fdccf7282ab009b3c06a8c63c93a61c8752b7cd8c94cf508c7ce6da98c53c39ef0f139fe7b8852692a2bbc193709774d5fdb736edc  py3-dnsrobocert-3.22.0.tar.gz
3a8f2d9a74a35aea2e5eebcede656d2861382c975dc94560eca4f94cd8b13f1bb4a98b5b667cb5937ef9123a8f1da20dcef58a8ffc903e93e979d928bca9f9b1  pebble.patch
"
