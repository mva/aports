# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kde-gtk-config
pkgver=5.26.0
pkgrel=0
pkgdesc="GTK2 and GTK3 Configurator for KDE"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-gtk-config"
license="GPL-2.0 AND LGPL-2.1-only OR LGPL-3.0-only"
depends="gsettings-desktop-schemas"
makedepends="
	extra-cmake-modules
	gsettings-desktop-schemas-dev
	gtk+2.0-dev
	gtk+3.0-dev
	karchive-dev
	kcmutils-dev
	kconfigwidgets-dev
	kdecoration-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	sassc
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-gtk-config-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1e3838ac9b3365ac59a40ac0c750ac04599c0c6bb948c62b16ba061619feb29b7f9886b580ee4df75061e50ce5575ec3c9a290393432db2fba53d5cb0d2cca16  kde-gtk-config-5.26.0.tar.xz
"
