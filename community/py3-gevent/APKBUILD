# Maintainer: Michael Zhou <zhoumichaely@gmail.com>
# Contributor: Michael Zhou <zhoumichaely@gmail.com>
pkgname=py3-gevent
pkgver=22.08.0
pkgrel=0
pkgdesc="Python3 library for seamless coroutine-based concurrency"
options="!check" # Requires unpackaged 'objgraph'
url="http://gevent.org/"
arch="all"
license="MIT"
depends="
	py3-greenlet
	py3-setuptools
	py3-zope-event
	py3-zope-interface
	"
makedepends="
	c-ares-dev
	cython
	file
	libev-dev
	libevent-dev
	libuv-dev
	python3-dev
	"
source="https://github.com/gevent/gevent/archive/$pkgver/gevent-$pkgver.tar.gz"
builddir="$srcdir/gevent-$pkgver"

replaces="py-gevent" # Backwards compatibility
provides="py-gevent=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	export GEVENTSETUP_EMBED=0
	CYTHON=cython PYTHON=python3 python3 setup.py build
}

package() {
	export GEVENTSETUP_EMBED=0
	python3 setup.py install --prefix=/usr --root="$pkgdir"

	rm -rf "$pkgdir"/usr/lib/python3*/site-packages/*/tests
}

sha512sums="
0655e36f7cbb4b6570e176a1d807ab0e2761a7812edf1367b459ecd6296fea601798fd0ba92dc418fadc0ab0b542df9f1478c81283750f3522b2c4c89dae8caf  gevent-22.08.0.tar.gz
"
