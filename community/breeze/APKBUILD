# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=breeze
pkgver=5.26.0
pkgrel=0
pkgdesc="Artwork, styles and assets for the Breeze visual style for the Plasma Desktop"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends_dev="
	kconfigwidgets-dev
	kdecoration-dev
	kguiaddons-dev
	ki18n-dev
	kiconthemes-dev
	kirigami2-dev
	kpackage-dev
	kwindowsystem-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/breeze-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
c2341e4ddeb5c5ae6c0923ecba93b7a3102cb49b68044c7a984bd83c7511fb9f68ddf462839895c97358ffb54e693aa456c477fa15d36424ec2f444e412c4c39  breeze-5.26.0.tar.xz
"
