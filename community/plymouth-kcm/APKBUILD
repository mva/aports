# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plymouth-kcm
pkgver=5.26.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/plymouth-kcm"
pkgdesc="KCM to manage the Plymouth (Boot) theme"
license="GPL-2.0-or-later"
depends="plymouth"
makedepends="
	extra-cmake-modules
	karchive-dev
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	plymouth-dev
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plymouth-kcm-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
0167f4b1a3f34b95ca514429f3d0eacb5069b3e34dc579e36347e1f42da710b318a4e1458b425533d021b3c5e71d5f3f94581d439a9fba5d0a2a66b019bb72d8  plymouth-kcm-5.26.0.tar.xz
"
